<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $guarded = [''];
    public static function boot()
    {
    parent::boot();
    static::creating(function($model) {
    $model->user_id = Auth()->user()->id;
    return true;
    });
    }
    public function user(){
    return $this->belongsTo('App\User');
    }
   
}
