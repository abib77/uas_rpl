<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Video;


class VideoController extends Controller
{
    public function __construct()
    {
    $this->middleware('auth');
    }
    public function index()
    {
    $videos = Video::paginate(10);
    return view('video.index', compact('videos'));
    }
    public function create()
    {
    return view('video.create');
    }
    public function store(Request $request)
    {
    $rules = [
    'title'=>'required|max:200',
    'youtube_id'=>'required',
    ];
    $this->validate($request,$rules);
    $video = Video::create($request->all());
    \Session::flash('flash_message', $video->title . ' Create
    Success');
    return redirect('video');
    }
    public function show(Video $video)
    {
    return view('video.show', compact('video'));
    }
    public function edit(Video $video)
    {
    return view('video.edit', compact('video'));
    }
    public function update(Request $request, Video $video)
    {;
    $video->update($request->all());
    $video->save();
    \Session::flash('flash_message', $video->title . ' Create
    Success');
    return redirect('video');
    }
    public function destroy(Video $video)
    {
    $video->delete();
    \Session::flash('flash_message', 'Delete Success');
    return redirect('video');
    }

}
