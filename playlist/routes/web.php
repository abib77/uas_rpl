<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $videos = App\Video::paginate(10);
    return view('welcome', compact('videos'));
   });
   Auth::routes();
   Route::get('/home', 'HomeController@index')->name('home');
   Route::resource('video', 'VideoController');
   Route::get('/{id}', function ($id) {
    $video = App\Video::where('youtube_id','=',$id)->first();
    $video->view += 1;
    $video->save();
    return view('video.show', compact('video'));
   });