@extends('layouts.app')
@section('content')
<div class="container">
 <div class="row justify-content-center">
 <div class="col-md-12">
 <div class="panel panel-default">
 <div class="panel-heading">Tambah Video</div>
 <div class="panel-body">
 {!! Form::model($video = new \App\Video,
['url'=>'/video']) !!}
 @if ($errors->any())
 {!! implode('', $errors->all('<div
class="alert alert-danger">:message</div>')) !!}
 @endif
 <div class="form-group">
 {!!Form::label('title', 'Judul Video')!!}
 {!!Form::text('title', null, ['class'=>'formcontrol','placeholder'=>'Judul
Video','required'])!!}
 </div>
 <div class="form-group">
 {!!Form::label('youtube_id', 'Youtube ID')!!}
 {!!Form::text('youtube_id', null,
['class'=>'form-control','placeholder'=>'Youtube ID','required'])!!}
 </div>
 <div class="form-group">
 {!!Form::label('description', 'Deskripsi')!!}
 {!!Form::textarea('description', null,
['class'=>'form-control','placeholder'=>'Deskripsi','required'])!!}
 </div>
 <button type="submit" class="btn-block btn btnprimary">Save
 </button>
 {!! Form::close()!!}
 </div>
 </div>
 </div>
 </div>
</div>
@endsection
