@extends('layouts.app')
@section('content')
<div class="container">
 <div class="row justify-content-center">
 <div class="col-md-12">
 <div class="panel">
 <div class="panel-heading">Detail Video</div>
 <div class="panel-body">
 <div class="row">
 <div class="col-md-12">
 <div class="videowrapper">

 <iframe width="100%"
src="https://www.youtube.com/embed/{{$video->youtube_id}}" frameborder="0"
allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-inpicture"
allowfullscreen></iframe>
 </div>
 </div>
 <div class="col-md-12">
 <ul class="list-group">
 <li class="list-group-item">
 Judul : {{$video->title}}
 </li>
 <li class="list-group-item">
 Deskripsi : {{$video->descripsion}}
 </li>
 <li class="list-group-item">
 Dilihat Sebanyak : {{$video->view}}
Kali
 </li>
 <li class="list-group-item">
 DiInput Oleh : {{$video->user->name}}
 </li>
 </ul>
 </div>
 </div>
 </div>
 </div>
 </div>
 </div>
</div>
@endsection