@extends('layouts.app')
@section('content')
<div class="container">
 <div class="row justify-content-center">
 <div class="col-md-12">
 <div class="panel panel-default">
 <div class="panel-heading">Video
 <a class="btn btn-sm btn-primary pull-right" href="
{{route('video.create')}}">Tambah Video</a>
 </div>
 <div class="panel-body">
 <table class="table">
 <thead>
 <tr>
 <th>id</th>
 <th>Title</th>
 <th>youtube_id</th>
 <th>View</th>
 <th width="180">Action</th>
 </tr>
 </thead>
 <tbody>
 @forelse ($videos as $video)
 <tr>
 <td>{{$video->id}}</td>
 <td>{{$video->title}}</td>
 <td>{{$video->youtube_id}}</td>
 <td>{{$video->view}}</td>
 <td>
 <a class="btn btn-sm btn-success"
href="{{route('video.show',$video->id)}}">Show</a>
 <a class="btn btn-sm btn-warning"
href="{{route('video.edit',$video->id)}}">Edit</a>
 {{ Form::open(array('url' => 'video/' .
$video->id, 'class' => 'pull-right')) }}
 {{ Form::hidden('_method', 'DELETE') }}
 {{ Form::submit('Delete', array('class' =>
    'btn btn-sm btn-danger btn-submit')) }}
 {{ Form::close() }}
 </td>
 </tr>
 @empty
 <tr><td colspan="3"> Data masih
kosong</td></tr>
 @endforelse
 </tbody>
 </table>
 {{$videos->links()}}
 </div>
 </div>
 </div>
 </div>
</div>
@endsection